<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'lingolang_db' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'IOaUT]!<B9++.%@@spY%oS={e?yLImB !2xp2b{mphaw>l{3lgSc)eyfFLKi$pHZ' );
define( 'SECURE_AUTH_KEY',  ',T6ZjW0#.MEZW@8jPoMkZ1/z&6^/w9zK:j_Dko)SD@rciD*a~kD~2HS{Tj2s_!;,' );
define( 'LOGGED_IN_KEY',    'f>s9#=y{/kn-ZI/e2n|~SpP@kg|8ON4D~l>/hT&n078N:M0)#KJj_,/>t#H5xZyZ' );
define( 'NONCE_KEY',        'R976.|Hp(l6?glVma4@lbx!XCB E`,$R&,0[~O(qLE!wJ]#mrBeXfm=?X2t-GW{L' );
define( 'AUTH_SALT',        '/Hh=L-T%eH^&*Kn`c8uAijXutUrjoLya9i6w^ b-UBEtDOw7Z q(^F._p{O`x]_s' );
define( 'SECURE_AUTH_SALT', '^jcA478RwGMKUR[n>;](%3x#{ Vd`^{iej`{>b)]Ve?XDObPb:>2G>,8$ev8qyg$' );
define( 'LOGGED_IN_SALT',   '(n}~J~u2V_W`v4DOHN1nwPGiQ<[)T$W[SzM@Gmijh5|1V)CjmpSRSMLULS5%e<~_' );
define( 'NONCE_SALT',       'qml~DcdP*uP7WX=9N?=LhPb,Tk/[TKmHNB_1,9]yh$}vnrFTL=SNcn>,+SNkva(8' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
