<?php
/*Template Name: 404*/
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package lingolang
 */

get_header();
?>

<main class="main_section error">
    <div class="container">
        <?php
            $page_404 =  get_page_by_title('404');
			echo $page_404->post_content;
            wp_reset_postdata();
        ?>
    </div>
</main>

<?php
get_footer();
