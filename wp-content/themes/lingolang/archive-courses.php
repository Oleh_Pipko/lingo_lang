<?php

get_header();
?>
<?php get_template_part( 'template-parts/breadcrumbs', get_post_type() ); ?>
<div class="page_title">
    <div class="container">
        <?php
            $post_title = 'Курсы';
            $post_type = $post->post_type;
            $currentPageID = get_the_ID();

            $post = get_post($post_id); 
            $slug = $post->post_name;

            $videosCount = wp_count_posts($post_type)->publish;
        ?>

        <h1 class="title"><?php echo $post_title; ?></h1>
        <?php 
        wp_nav_menu( [
            'menu'            => $post_title, 
            'container'       => 'div', 
            'container_class' => 'course_category',
            'menu_class'      => 'category_list container_flex', 
        ] );
        ?>
    </div>
</div>
<main class="video_lessons_page main_section courses free_lessons">
    <section class="courses_wrap">
        <div class="container">
            <div class="subtitle">
                <h3>Все видео <sup class="quantity"><?php echo $videosCount >= 10 ? $videosCount : '0' . $videosCount; ?></sup></h3>
            </div>
            <div class="courses_list container_flex">
                <?php
                if ( have_posts() ) :
                    $i = 0;

                    while ( have_posts() ) :
                        the_post();
                ?>
                <div class="course_item">
                    <div class="image_wrap">
                        <div class="level">
                            <svg class="flag_icon" viewBox="0 0 119 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect width="119" height="50" fill="#EB2136"/>
                                <path d="M44.4534 18.4286L47.402 22.4918L52.1772 20.9434L49.224 25.0031L52.1723 29.0663L47.3986 27.5122L44.4455 31.5718L44.4485 26.5518L39.6748 24.9975L44.4502 23.4491L44.4534 18.4286Z"
                                      fill="white"/>
                                <path d="M36.4061 35.1565C30.797 35.1565 26.2498 30.6094 26.2498 25.0001C26.2498 19.3908 30.7969 14.8438 36.4061 14.8438C38.155 14.8438 39.8006 15.286 41.2374 16.0644C38.9836 13.8604 35.9012 12.5 32.5 12.5C25.5965 12.5 20 18.0965 20 25C20 31.9035 25.5965 37.5 32.5 37.5C35.9012 37.5 38.9836 36.1396 41.2374 33.9356C39.8006 34.7142 38.155 35.1565 36.4061 35.1565Z"
                                      fill="white"/>
                            </svg>
                            <p><?php echo get_the_terms($post->ID, 'levels_courses')[0]->name; ?></p>
                        </div>
                        <div class="image" style="background-image: url('<?php the_post_thumbnail_url(); ?>')"></div>
                        <div class="title_wrap">
                            <h3><?php the_title(); ?></h3>
                        </div>
                    </div>
                    <div class="course_info">
                        <div class="price_wrap">
                            <p>Цена за курс:</p>
                            <span class="price"><?php 
                                $coursePrice = get_field("course_price"); 
                                $coursePriceLenght = strlen($coursePrice);
                                $coursePriceFirstLenght = $coursePriceLenght % 3;
                                for ($i = 0; $i < $coursePriceFirstLenght; $i++) {
                                    echo $coursePrice[$i];
                                }
                                for ($i = $coursePriceFirstLenght; $i < $coursePriceLenght; $i += 3) {
                                    echo ' ' . $coursePrice[$i] . $coursePrice[$i + 1] . $coursePrice[$i + 2];
                                }  ?><span class="currency">&nbsp;руб.</span></span>
                        </div>
                        <div class="course_composition">
                            <div class="modules">
                                <span><?php the_field("modules_count"); ?> модулей</span>
                            </div>
                            <div class="lessons">
                                <span><?php the_field("lessons_count"); ?> уроков</span>
                            </div>
                        </div>
                    </div>
                    <div class="buttons container_flex">
                        <a href="#" class="buy_btn">Купить</a>
                        <a href="#course-<?php echo $i; ?>" class="skype_btn about">О курсе</a>
                    </div>
                    <div class="course_item_modal about_course no-display" id="course-<?php echo $i; ?>">
                        <div class="content">
                            <div class="about_course">
                                <div class="title">
                                    <h3>О курсе</h3>
                                </div>
                                <div class="description">
                                    <?php the_content(); ?>
                                </div>
                                <a href="#" class="buy_btn">Купить курс</a>
                            </div>
                            <div class="image_wrap">
                                <div class="image" style="background-image: linear-gradient(180deg, rgba(255, 255, 255, 0.2) 50%, rgba(34,34,34,0.9) 100%), url('<?php the_post_thumbnail_url(); ?>')"></div>
                                <div class="course_info">
                                    <div class="level">
                                        <svg class="flag_icon" viewBox="0 0 119 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <rect width="119" height="50" fill="#EB2136"/>
                                            <path d="M44.4534 18.4286L47.402 22.4918L52.1772 20.9434L49.224 25.0031L52.1723 29.0663L47.3986 27.5122L44.4455 31.5718L44.4485 26.5518L39.6748 24.9975L44.4502 23.4491L44.4534 18.4286Z"
                                                fill="white"/>
                                            <path d="M36.4061 35.1565C30.797 35.1565 26.2498 30.6094 26.2498 25.0001C26.2498 19.3908 30.7969 14.8438 36.4061 14.8438C38.155 14.8438 39.8006 15.286 41.2374 16.0644C38.9836 13.8604 35.9012 12.5 32.5 12.5C25.5965 12.5 20 18.0965 20 25C20 31.9035 25.5965 37.5 32.5 37.5C35.9012 37.5 38.9836 36.1396 41.2374 33.9356C39.8006 34.7142 38.155 35.1565 36.4061 35.1565Z"
                                                fill="white"/>
                                        </svg>
                                        <p><?php echo get_the_terms($post->ID, 'levels_courses')[0]->name; ?></p>
                                    </div>
                                    <div class="title_wrap">
                                        <h3><?php the_title();?></h3>
                                    </div>
                                    <div class="course_composition">
                                        <div class="modules">
                                            <span><?php the_field("modules_count"); ?> модулей</span>
                                        </div>
                                        <div class="lessons">
                                            <span><?php the_field("lessons_count"); ?> уроков</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                        $i++;
                    endwhile;
                endif;
                wp_reset_postdata();
                ?>
            </div>
            <div class="pagination">
                <ul class="page_numbers container_flex" pagination_count="<?php the_field("pagination_count"); ?>">
                    <?php
                    echo paginate_links( array(
                        'before_page_number' => '<li><span class="page_number">',
                        'after_page_number' => '</span></li>',
                        'prev_text' => '<li><span class="page_number prev" style="transform: rotateZ(180deg);">
                        <svg viewBox="0 0 6 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1.14031 0.13528L0.86109 0.412568C0.773892 0.499835 0.725853 0.61594 0.725853 0.740028C0.725853 0.864047 0.773892 0.980289 0.86109 1.06756L4.02434 4.23067L0.857581 7.39743C0.770382 7.48456 0.722412 7.6008 0.722412 7.72482C0.722412 7.84884 0.770382 7.96515 0.857581 8.05235L1.13508 8.3297C1.31553 8.5103 1.60947 8.5103 1.78993 8.3297L5.57389 4.5593C5.66102 4.47217 5.72241 4.35606 5.72241 4.23094L5.72241 4.2295C5.72241 4.10541 5.66095 3.98931 5.57389 3.90218L1.80018 0.13528C1.71305 0.0480117 1.59344 0.000110588 1.46942 -2.67355e-05C1.34533 -2.67301e-05 1.22737 0.0480118 1.14031 0.13528Z"/>
                        </svg>
                    </span></li>',
                        'next_text' => '<li><span class="page_number next">
                        <svg viewBox="0 0 6 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1.14031 0.13528L0.86109 0.412568C0.773892 0.499835 0.725853 0.61594 0.725853 0.740028C0.725853 0.864047 0.773892 0.980289 0.86109 1.06756L4.02434 4.23067L0.857581 7.39743C0.770382 7.48456 0.722412 7.6008 0.722412 7.72482C0.722412 7.84884 0.770382 7.96515 0.857581 8.05235L1.13508 8.3297C1.31553 8.5103 1.60947 8.5103 1.78993 8.3297L5.57389 4.5593C5.66102 4.47217 5.72241 4.35606 5.72241 4.23094L5.72241 4.2295C5.72241 4.10541 5.66095 3.98931 5.57389 3.90218L1.80018 0.13528C1.71305 0.0480117 1.59344 0.000110588 1.46942 -2.67355e-05C1.34533 -2.67301e-05 1.22737 0.0480118 1.14031 0.13528Z"/>
                        </svg>
                    </span></li>',
                    ));
                    ?>
                    <?php  wp_reset_postdata(); ?>
                </ul>
            </div>
        </div>
    </section>
</main>

<?php
get_footer();
