<?php

get_header();
?>
<?php get_template_part( 'template-parts/breadcrumbs', get_post_type() ); ?>
<div class="skype_page">
    <div class="presentation container container_flex">
        <div class="title_side">
            <div class="flag">
                <svg class="flag_icon" viewBox="0 0 400 200" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect width="400" height="200" fill="#EB2136"/>
                    <path d="M158.814 73.7147L170.608 89.9672L189.709 83.7736L177.896 100.013L189.689 116.265L170.594 110.049L158.782 126.287L158.794 106.207L139.699 99.9899L158.801 93.7963L158.814 73.7147Z"
                          fill="white"/>
                    <path d="M126.625 140.626C104.188 140.626 85.9993 122.438 85.9993 100.001C85.9993 77.5634 104.187 59.3752 126.625 59.3752C133.62 59.3752 140.202 61.1439 145.95 64.2576C136.934 55.4415 124.605 50 111 50C83.3859 50 61 72.3859 61 100C61 127.614 83.3859 150 111 150C124.605 150 136.934 144.559 145.95 135.742C140.202 138.857 133.62 140.626 126.625 140.626Z"
                          fill="white"/>
                </svg>
            </div>
            <h1><?php the_field("heading", "option"); ?></h1>
        </div>
        <?php if (have_rows("skype_video", "option")) : the_row(); ?>
        <div class="video_block">
            <div class="substrate">
                <span class="play_icon"></span>
                <a data-fancybox href="<?php the_sub_field("file"); ?>" class="video_screen">
                    <img src="<?php the_sub_field("preview"); ?>" alt="<?php the_sub_field("description"); ?>">
                </a>
            </div>
            <div class="short_descr">
                <p><?php the_sub_field("description"); ?></p>
            </div>
        </div>
        <?php endif; ?>
    </div>

</div>
<div class="skype_page">
    <div class="page_title container">
        <?php
            $post_title = 'Занятия по Skype';
            $post_type = $post->post_type;
            $currentPageID = get_the_ID();

            $post = get_post($post_id); 
            $slug = $post->post_name;

            $videosCount = wp_count_posts($post_type)->publish;
        ?>

        <h1 class="title"><?php echo $post_title; ?></h1>
        <?php 
        wp_nav_menu( [
            'menu'            => $post_title, 
            'container'       => 'div', 
            'container_class' => 'course_category',
            'menu_class'      => 'category_list container_flex', 
        ] );
        ?>
    </div>
</div>
<main class="main_section courses skype_courses">
    <section class="courses_wrap">
        <div class="container">
            <div class="subtitle">
                <h3>Все видео <sup class="quantity"><?php echo $videosCount >= 10 ? $videosCount : '0' . $videosCount; ?></sup></h3>
            </div>
            <div class="courses_list container_flex">
                <?php
                if ( have_posts() ) :
                    $i = 0;

                    while ( have_posts() ) :
                        the_post();
                        $queried_object = get_the_terms($post->ID, 'levels_skype_lessons')[0];
                        $taxonomy = $queried_object->taxonomy;
                        $term_id = $queried_object->term_id;
                ?>
                <div class="course_item">
                    <div class="image_wrap">
                        <div class="level">
                            <p>Уровень <?php the_field('skype_lessons_level_title', $taxonomy . '_' .$term_id); ?></p>
                        </div>
                        <div class="image" style="background-image: url('<?php the_post_thumbnail_url(); ?>')"></div>
                        <div class="title_wrap">
                            <h3><?php the_title(); ?></h3>
                        </div>
                    </div>
                    <div class="course_info">
                        <div class="schedule">
                            <p>Время проведения:</p>
                            <span class="date"><?php the_field("date_first"); ?> - <?php the_field("date_last"); ?></span>
                            <span class="time"><?php the_field("time_first"); ?> - <?php the_field("time_last"); ?></span>
                        </div>
                        <div class="course_composition">
                            <div class="modules">
                                <span><?php the_field("modules_count"); ?> модулей</span>
                            </div>
                            <div class="lessons">
                                <span><?php the_field("lessons_count"); ?> уроков</span>
                            </div>
                        </div>
                    </div>
                    <div class="buttons container_flex">
                        <div class="price_wrap">
                            <p>Цена за весь курс:</p>
                            <span class="price"><?php 
                                $coursePrice = get_field("course_price"); 
                                $coursePriceLenght = strlen($coursePrice);
                                $coursePriceFirstLenght = $coursePriceLenght % 3;
                                for ($i = 0; $i < $coursePriceFirstLenght; $i++) {
                                    echo $coursePrice[$i];
                                }
                                for ($i = $coursePriceFirstLenght; $i < $coursePriceLenght; $i += 3) {
                                    echo ' ' . $coursePrice[$i] . $coursePrice[$i + 1] . $coursePrice[$i + 2];
                                }
                            ?><span class="currency">&nbsp;руб.</span></span>
                        </div>
                        <a href="#" class="buy_btn">Участвовать</a>
                    </div>
                </div>
                <?php
                        $i++;
                    endwhile;
                endif;
                wp_reset_postdata();
                ?>
            </div>
            <div class="pagination">
                <ul class="page_numbers container_flex">
                    <?php
                    echo paginate_links( array(
                        'before_page_number' => '<li><span class="page_number">',
                        'after_page_number' => '</span></li>',
                        'prev_text' => '<li><span class="page_number prev" style="transform: rotateZ(180deg);">
                        <svg viewBox="0 0 6 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1.14031 0.13528L0.86109 0.412568C0.773892 0.499835 0.725853 0.61594 0.725853 0.740028C0.725853 0.864047 0.773892 0.980289 0.86109 1.06756L4.02434 4.23067L0.857581 7.39743C0.770382 7.48456 0.722412 7.6008 0.722412 7.72482C0.722412 7.84884 0.770382 7.96515 0.857581 8.05235L1.13508 8.3297C1.31553 8.5103 1.60947 8.5103 1.78993 8.3297L5.57389 4.5593C5.66102 4.47217 5.72241 4.35606 5.72241 4.23094L5.72241 4.2295C5.72241 4.10541 5.66095 3.98931 5.57389 3.90218L1.80018 0.13528C1.71305 0.0480117 1.59344 0.000110588 1.46942 -2.67355e-05C1.34533 -2.67301e-05 1.22737 0.0480118 1.14031 0.13528Z"/>
                        </svg>
                    </span></li>',
                        'next_text' => '<li><span class="page_number next">
                        <svg viewBox="0 0 6 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1.14031 0.13528L0.86109 0.412568C0.773892 0.499835 0.725853 0.61594 0.725853 0.740028C0.725853 0.864047 0.773892 0.980289 0.86109 1.06756L4.02434 4.23067L0.857581 7.39743C0.770382 7.48456 0.722412 7.6008 0.722412 7.72482C0.722412 7.84884 0.770382 7.96515 0.857581 8.05235L1.13508 8.3297C1.31553 8.5103 1.60947 8.5103 1.78993 8.3297L5.57389 4.5593C5.66102 4.47217 5.72241 4.35606 5.72241 4.23094L5.72241 4.2295C5.72241 4.10541 5.66095 3.98931 5.57389 3.90218L1.80018 0.13528C1.71305 0.0480117 1.59344 0.000110588 1.46942 -2.67355e-05C1.34533 -2.67301e-05 1.22737 0.0480118 1.14031 0.13528Z"/>
                        </svg>
                    </span></li>',
                    ));
                    ?>
                    <?php  wp_reset_postdata(); ?>
                </ul>
            </div>
        </div>
    </section>
</main>

<?php
get_footer();
?>
<script>
$(".menu-item-object-skype_lessons").addClass("current-menu-item");
</script>