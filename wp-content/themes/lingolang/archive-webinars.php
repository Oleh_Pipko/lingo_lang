<?php

get_header();
?>
<?php get_template_part( 'template-parts/breadcrumbs', get_post_type() ); ?>
<div class="page_title">
    <div class="container">
        <?php
            $post_title = 'Вебинары';
            $post_type = $post->post_type;
            $currentPageID = get_the_ID();

            $post = get_post($post_id); 
            $slug = $post->post_name;

            $videosCount = wp_count_posts($post_type)->publish;
        ?>

        <h1 class="title"><?php echo $post_title; ?></h1>
        <?php 
        wp_nav_menu( [
            'menu'            => $post_title, 
            'container'       => 'div', 
            'container_class' => 'course_category',
            'menu_class'      => 'category_list container_flex', 
        ] );
        ?>
    </div>
</div>
<main class="video_lessons_page main_section courses webinars_page">
    <section class="courses_wrap webinar_lessons">
        <div class="container">
            <div class="subtitle">
                <h3>Все видео <sup class="quantity"><?php echo $videosCount >= 10 ? $videosCount : '0' . $videosCount; ?></sup></h3>
            </div>
            <div class="video_lessons_list container_flex">
                <?php
                if ( have_posts() ) :

                    while ( have_posts() ) :
                        the_post();
                ?>
                <div class="video_lesson_item">
                    <div class="video_block">
                        <div class="substrate">
                            <span class="play_icon"></span>
                            <a data-fancybox href="<?php the_field("file"); ?>" class="video_screen">
                                <img src="<?php the_post_thumbnail_url(); ?>" alt="video-screen">
                            </a>
                            <?php if (get_field("duration")) { ?>
                                <span class="duration">
                                <?php  
                                $durationText = get_field("duration"); 
                                if ($durationText[0] == 0 && $durationText[1] == 0) {
                                    for ($i = 3; $i < strlen($durationText); $i++) {
                                        echo $durationText[$i];
                                    }
                                }
                                else echo $durationText;
                                ?></span>
                            <?php } ?>
                            <span class="video_scale"><span class="progress_bar"></span></span>
                        </div>
                    </div>
                    <div class="video_info">
                        <div class="lesson_title">
                            <h3>
                            <?php 
                                $videoTitle = get_the_title();
                                if (strlen($videoTitle) > 160) {
                                    for ($i = 0; $i < 160; $i++) {
                                        echo $videoTitle[$i];
                                    }
                                    echo "...";
                                }
                                else echo $videoTitle
                            ?>
                            </h3>
                        </div>
                        <div class="date"><span><?php the_time('d M Y'); ?></span></div>
                    </div>
                </div>
                <?php
                    endwhile;
                endif;
                wp_reset_postdata();
                ?>
            </div>
            <div class="pagination">
                <ul class="page_numbers container_flex" pagination_count="<?php the_field("pagination_count"); ?>">
                    <?php
                    echo paginate_links( array(
                        'before_page_number' => '<li><span class="page_number">',
                        'after_page_number' => '</span></li>',
                        'prev_text' => '<li><span class="page_number prev" style="transform: rotateZ(180deg);">
                        <svg viewBox="0 0 6 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1.14031 0.13528L0.86109 0.412568C0.773892 0.499835 0.725853 0.61594 0.725853 0.740028C0.725853 0.864047 0.773892 0.980289 0.86109 1.06756L4.02434 4.23067L0.857581 7.39743C0.770382 7.48456 0.722412 7.6008 0.722412 7.72482C0.722412 7.84884 0.770382 7.96515 0.857581 8.05235L1.13508 8.3297C1.31553 8.5103 1.60947 8.5103 1.78993 8.3297L5.57389 4.5593C5.66102 4.47217 5.72241 4.35606 5.72241 4.23094L5.72241 4.2295C5.72241 4.10541 5.66095 3.98931 5.57389 3.90218L1.80018 0.13528C1.71305 0.0480117 1.59344 0.000110588 1.46942 -2.67355e-05C1.34533 -2.67301e-05 1.22737 0.0480118 1.14031 0.13528Z"/>
                        </svg>
                    </span></li>',
                        'next_text' => '<li><span class="page_number next">
                        <svg viewBox="0 0 6 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1.14031 0.13528L0.86109 0.412568C0.773892 0.499835 0.725853 0.61594 0.725853 0.740028C0.725853 0.864047 0.773892 0.980289 0.86109 1.06756L4.02434 4.23067L0.857581 7.39743C0.770382 7.48456 0.722412 7.6008 0.722412 7.72482C0.722412 7.84884 0.770382 7.96515 0.857581 8.05235L1.13508 8.3297C1.31553 8.5103 1.60947 8.5103 1.78993 8.3297L5.57389 4.5593C5.66102 4.47217 5.72241 4.35606 5.72241 4.23094L5.72241 4.2295C5.72241 4.10541 5.66095 3.98931 5.57389 3.90218L1.80018 0.13528C1.71305 0.0480117 1.59344 0.000110588 1.46942 -2.67355e-05C1.34533 -2.67301e-05 1.22737 0.0480118 1.14031 0.13528Z"/>
                        </svg>
                    </span></li>',
                    ));
                    ?>
                    <?php  wp_reset_postdata(); ?>
                </ul>
            </div>
        </div>
    </section>
</main>

<?php
get_footer();
