<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package lingolang
 */

get_header();
?>

	<main id="primary" class="site-main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<?php
				the_archive_title( '<h1 class="page-title">', '</h1>' );
				the_archive_description( '<div class="archive-description">', '</div>' );
				?>
			</header><!-- .page-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_type() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

	</main><!-- #main -->
	<div class="overlay_content"></div>
<div class="breadcrumds_wrapper">
    <div class="container">
        <div class="breadcrumbs">
            <ul class="breadcrumb">
                <?php
                    if(function_exists('bcn_display'))
                    {
                    bcn_display();
                }?>
            </ul>
        </div>
    </div>
</div>
<div class="page_title">
    <div class="container">
        <h1 class="title"><?php single_post_title(); ?></h1>
        <div class="course_category">
            <ul class="category_list container_flex">
                <li class="category_item current"><a href="#">Все</a></li>
                <?php 
                    $all_categories = get_categories('hide_empty=1');
                    $category_link_array = array();
                    foreach( $all_categories as $single_cat ){
                        $category_link_array[] = '<li class="category_item <!--current-->"><a href="' . get_category_link($single_cat->term_id) . '">' . $single_cat->name . '</a></li>';
                    }
                    echo implode('', $category_link_array); 
                ?>
                <li class="category_item more"><a href="#"></a></li>
            </ul>
        </div>
    </div>
</div>
<main class="main_section courses blog">
    <section class="courses_wrap">
        <div class="container">
            <div class="subtitle">
                <h3>Все направления <sup class="quantity"><?php wp_count_posts(); ?></sup></h3>
            </div>
            <div class="blog_list container_flex">
                    <?php if ( have_posts() ) {

                        $i = 0;
                        
                        while ( have_posts() ) {
                            $i++;
                            if ( $i > 1 ) {
                                echo '<hr class="post-separator styled-separator is-style-wide section-inner" aria-hidden="true" />';
                            }
                            the_post();
                        
                            get_template_part( 'template-parts/blog_post_content', get_post_type() );
                        
                        }
                    }
                    wp_reset_postdata();
                ?>
            </div>
            <div class="pagination">
                <ul class="page_numbers container_flex">
                    <?php echo paginate_links( array(
                        'before_page_number' => '<li><span class="page_number">',
                        'after_page_number' => '</span></li>',
                        'prev_text' => '<li><span class="page_number prev" style="transform: rotateZ(180deg);">
                        <svg viewBox="0 0 6 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1.14031 0.13528L0.86109 0.412568C0.773892 0.499835 0.725853 0.61594 0.725853 0.740028C0.725853 0.864047 0.773892 0.980289 0.86109 1.06756L4.02434 4.23067L0.857581 7.39743C0.770382 7.48456 0.722412 7.6008 0.722412 7.72482C0.722412 7.84884 0.770382 7.96515 0.857581 8.05235L1.13508 8.3297C1.31553 8.5103 1.60947 8.5103 1.78993 8.3297L5.57389 4.5593C5.66102 4.47217 5.72241 4.35606 5.72241 4.23094L5.72241 4.2295C5.72241 4.10541 5.66095 3.98931 5.57389 3.90218L1.80018 0.13528C1.71305 0.0480117 1.59344 0.000110588 1.46942 -2.67355e-05C1.34533 -2.67301e-05 1.22737 0.0480118 1.14031 0.13528Z"/>
                        </svg>
                    </span></li>',
                        'next_text' => '<li><span class="page_number next">
                        <svg viewBox="0 0 6 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1.14031 0.13528L0.86109 0.412568C0.773892 0.499835 0.725853 0.61594 0.725853 0.740028C0.725853 0.864047 0.773892 0.980289 0.86109 1.06756L4.02434 4.23067L0.857581 7.39743C0.770382 7.48456 0.722412 7.6008 0.722412 7.72482C0.722412 7.84884 0.770382 7.96515 0.857581 8.05235L1.13508 8.3297C1.31553 8.5103 1.60947 8.5103 1.78993 8.3297L5.57389 4.5593C5.66102 4.47217 5.72241 4.35606 5.72241 4.23094L5.72241 4.2295C5.72241 4.10541 5.66095 3.98931 5.57389 3.90218L1.80018 0.13528C1.71305 0.0480117 1.59344 0.000110588 1.46942 -2.67355e-05C1.34533 -2.67301e-05 1.22737 0.0480118 1.14031 0.13528Z"/>
                        </svg>
                    </span></li>',
                    )); ?>
                </ul>
            </div>
        </div>
    </section>
</main>

<?php
get_sidebar();
get_footer();
