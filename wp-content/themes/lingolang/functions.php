<?php
/**
 * lingolang functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package lingolang
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'lingolang_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function lingolang_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on lingolang, use a find and replace
		 * to change 'lingolang' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'lingolang', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'lingolang' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'lingolang_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'lingolang_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function lingolang_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'lingolang_content_width', 640 );
}
add_action( 'after_setup_theme', 'lingolang_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function lingolang_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'lingolang' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'lingolang' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'lingolang_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function lingolang_scripts() {
	wp_enqueue_style( 'fancybox-style', get_template_directory_uri() . "/assets/css/jquery.fancybox.min.css", array(), _S_VERSION );
	wp_enqueue_style( 'fancybox-style', get_template_directory_uri() . "/assets/css/fonts.css", array(), _S_VERSION );
	wp_enqueue_style( 'main-style', get_template_directory_uri() . "/assets/css/style.css", array(), _S_VERSION );
	wp_enqueue_style( 'adaptive-style', get_template_directory_uri() . "/assets/css/adaptive.css", array(), _S_VERSION );

	wp_enqueue_style( 'lingolang-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'lingolang-style', 'rtl', 'replace' );

	wp_enqueue_script( 'jquery-js', get_template_directory_uri() . '/assets/js/jquery-2.2.4.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'fancybox-js', get_template_directory_uri() . '/assets/js/jquery.fancybox.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'main-js', get_template_directory_uri() . '/assets/js/main.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'lingolang-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'lingolang-customizer', get_template_directory_uri() . '/js/customizer.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'lingolang_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Настройки темы',
		'menu_title'	=> 'Настройки темы',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_page(array(
		'page_title' 	=> 'Занятия по Skype (баннер)',
		'menu_title'	=> 'Занятия по Skype (баннер)',
		'menu_slug' 	=> 'skype-courses-settings'
	));
	
}

function mayak_nav_menu_no_link($no_link){
	$gg_mk = '!<li(.*?)class="(.*?)current_page_item(.*?)"><a(.*?)>(.*?)</a>!si';
	$dd_mk = '<li$1class="\\2current_page_item\\3"><span>$5</span>';
	return preg_replace($gg_mk, $dd_mk, $no_link );
}
add_filter('wp_nav_menu', 'mayak_nav_menu_no_link');



function hwl_home_pagesize( $query ) {
	if ( is_post_type_archive( 'free_lessons' ) ) {
		$query->set( 'posts_per_page', 9 );
		return;
	}

	if ( is_post_type_archive( 'webinars' ) ) {
		$query->set( 'posts_per_page', 9 );
		return;
	}

	if ( is_post_type_archive( 'courses' ) ) {
		$query->set( 'posts_per_page', 4 );
		return;
	}

	if ( is_post_type_archive( 'skype_lessons' ) ) {
		$query->set( 'posts_per_page', 4 );
		return;
	}

	if ( is_post_type_archive( 'grammar' ) ) {
		$query->set( 'posts_per_page', 3 );
		return;
	}
}
add_action('pre_get_posts', 'hwl_home_pagesize', 1 );