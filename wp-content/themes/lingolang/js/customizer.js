/* global wp, jQuery */
/**
 * File customizer.js.
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */
/*
( function( $ ) {
	// Site title and description.
	wp.customize( 'blogname', function( value ) {
		value.bind( function( to ) {
			$( '.site-title a' ).text( to );
		} );
	} );
	wp.customize( 'blogdescription', function( value ) {
		value.bind( function( to ) {
			$( '.site-description' ).text( to );
		} );
	} );

	// Header text color.
	wp.customize( 'header_textcolor', function( value ) {
		value.bind( function( to ) {
			if ( 'blank' === to ) {
				$( '.site-title, .site-description' ).css( {
					clip: 'rect(1px, 1px, 1px, 1px)',
					position: 'absolute',
				} );
			} else {
				$( '.site-title, .site-description' ).css( {
					clip: 'auto',
					position: 'relative',
				} );
				$( '.site-title a, .site-description' ).css( {
					color: to,
				} );
			}
		} );
	} );
}( jQuery ) );
*/


// Бесплатные уроки
/*
var paginationPos;
function showFCelements (minPos) {
	var showCount = 9;
	showCount += minPos;
	var itemsCount = $(".video_lesson_item").length;
	for (var i = 0; i < itemsCount; i++) {
		if (i >= minPos && i < showCount) {
			$($(".video_lesson_item")[i]).css("display", "block");
		}
		else {
			$($(".video_lesson_item")[i]).css("display", "none");
		}
	}

	paginationPos = minPos / 9 + 1;
}

$(".pagination .page_number").click(function() {
	var paginationsBtnsCount = $(".pagination .page_numbers").attr("pagination_count");

	if (!($(this).hasClass("next")) && !($(this).hasClass("prev"))) {
		$(".pagination .page_number").removeClass("current");
		$(this).addClass("current");
	}
	else {
		$(".pagination .page_number").removeClass("current");
		for (var i = 1; i < $(".page_number").length - 1; i++) {
			if ($($(".page_number")[i]).text() == (
				$(this).hasClass("prev") ? paginationPos - 1 : paginationPos + 1
			)) {
				$($(".page_number")[i]).addClass("current");
				break;
			}
		}
	}
	showFCelements(($(".page_number.current").text() - 1) * 9);

	if (paginationsBtnsCount) {
		$(".page_number").parent().css("display", "none");

		$($(".page_number")[0]).parent().css("display", "block");

		var spaceLeft = paginationPos - Math.floor(paginationsBtnsCount / 2);
		var spaceRight = paginationPos + Math.floor(paginationsBtnsCount / 2);

		if (spaceLeft <= 0) {
			while (spaceLeft <= 0) {
				spaceLeft++;
				spaceRight++;
			}
		}

		if (paginationsBtnsCount % 2) spaceRight++;
		
		if (spaceRight > $(".page_number").length - 1) {
			while (spaceRight > $(".page_number").length - 1) {
				spaceLeft--;
				spaceRight--;
			}
		}
		
		for (var j = spaceLeft; j < (paginationsBtnsCount % 2 ? spaceRight : spaceRight); j++)
			$($(".page_number")[j]).parent().css("display", "block");

		$($(".page_number")[$(".page_number").length - 1]).parent().css("display", "block");
	}

	if ($(".page_number.current").text() == 1) {
		$(".page_number.prev").css("display", "none");
		$(".page_number.next").css("display", "flex");
	}
	else if ($(".page_number.current").text() == $(".page_number").length - 2) {
		$(".page_number.prev").css("display", "flex");
		$(".page_number.next").css("display", "none");
	}
	else {
		$(".page_number.prev").css("display", "flex");
		$(".page_number.next").css("display", "flex");
	}
});

showFCelements(0);
$($(".video_lessons_page .pagination .page_number")[1]).click();
*/

history.pushState('', document.title, window.location.pathname);

var pageURL = window.location.href;
var pageLinks = document.querySelectorAll('a');

pageLinks.forEach(function(pageLink) {
	if (pageLink.getAttribute('href') == pageURL) {
		pageLink.removeAttribute('href');
	}
});