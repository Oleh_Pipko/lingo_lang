<?php
/*Template Name: Домашняя страница*/
get_header();
?>
<main class="main_section" style="background-image: url('<?php the_post_thumbnail_url(); ?>')">
    <?php if ( have_posts() ) : ?>
    <div class="flag">
        <div class="container">
            <div class="slogan">
                <?php the_content(); ?>
            </div>
            <svg class="flag_icon" viewBox="0 0 400 200" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect width="400" height="200" fill="#EB2136"/>
                <path d="M158.814 73.7147L170.608 89.9672L189.709 83.7736L177.896 100.013L189.689 116.265L170.594 110.049L158.782 126.287L158.794 106.207L139.699 99.9899L158.801 93.7963L158.814 73.7147Z"
                      fill="white"/>
                <path d="M126.625 140.626C104.188 140.626 85.9993 122.438 85.9993 100.001C85.9993 77.5634 104.187 59.3752 126.625 59.3752C133.62 59.3752 140.202 61.1439 145.95 64.2576C136.934 55.4415 124.605 50 111 50C83.3859 50 61 72.3859 61 100C61 127.614 83.3859 150 111 150C124.605 150 136.934 144.559 145.95 135.742C140.202 138.857 133.62 140.626 126.625 140.626Z"
                      fill="white"/>
            </svg>
        </div>
    </div>
    <div class="presentation">
        <div class="container container_flex">
            <div class="title_side">
                <h1><?php single_post_title(); ?></h1>

                <?php 
                    if (have_rows("btns")) :
                ?>
                <div class="buttons container_flex">
                    <?php 
                        while(have_rows("btns")) : the_row();
                    ?>
                    <a href="<?php the_sub_field("link"); ?>" class="<?php the_sub_field("type"); ?>"><?php the_sub_field("text"); ?></a>
                    <?php endwhile; ?>
                </div>
            <?php endif; ?>
            </div>
            <?php if (have_rows("video")) : the_row(); ?>
            <div class="video_block">
                <div class="substrate">
                    <span class="play_icon"></span>
                    <a data-fancybox href="<?php the_sub_field("file"); ?>" class="video_screen">
                        <img src="<?php the_sub_field("preview"); ?>" alt="video-screen">
                    </a>
                </div>
                <div class="short_descr">
                    <p><?php the_sub_field("description"); ?></p>
                </div>
            </div>
            <?php endif; ?>
        </div>

    </div>
    <?php endif; ?>
</main>
<?php 
get_footer();
?>