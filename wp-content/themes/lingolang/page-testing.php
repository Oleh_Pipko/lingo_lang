<?php
/*Template Name: Тестирование*/

get_header();
?>
<div class="breadcrumds_wrapper">
    <div class="container">
        <div class="breadcrumbs">
            <ul class="breadcrumb">
                <?php
                    if(function_exists('bcn_display'))
                    {
                    bcn_display();
                }?>
            </ul>
        </div>
    </div>
</div>
<div class="page_title">
    <div class="container">
        <h1 class="title"><?php the_title(); ?></h1>
        <div class="course_category">
            <ul class="category_list container_flex">
                <li class="category_item current"><a href="#" onclick="rewriteTestingFunc(0)">Вопросы для начинающих</a></li>
                <li class="category_item"><a href="#" onclick="rewriteTestingFunc(1)">Вопросы продвинутый уровень</a></li>
            </ul>
        </div>
    </div>
</div>
<main class="main_section online_test">
    <div class="container">
        <div class="test_block test_questions">
            <div class="questions_side">
                <div class="questions_qty">
                    <span class="current_number"></span>
                    <span>/</span>
                    <span class="total"></span>
                    <p>Осталось еще пару вопросов</p>
                </div>
                <div class="progress_bar">
                    <span class="current_progress"></span>
                </div>
                <div class="questions">
                    <div class="current_question">
                        <span></span>
                    </div>
                    <div class="question_wrap">
                        <h2></h2>
                        <div class="options">
                            <label class="option">
                                <input type="radio" name="test_option" class="check" id="test_option_1" checked>
                                <span></span>
                            </label>
                            <label class="option">
                                <input type="radio" name="test_option" class="check" id="test_option_2">
                                <span></span>
                            </label>
                            <label class="option">
                                <input type="radio" name="test_option" class="check" id="test_option_3">
                                <span></span>
                            </label>
                            <label class="option">
                                <input type="radio" name="test_option" class="check" id="test_option_4">
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <button onclick="testingFunc();" class="next_btn">Следующий вопрос</button>
            </div>
            </div>
            <div class="instruction">
                <?php the_content(); ?>
            </div>
        </div>
        <div class="test_block test_result">
            <div class="title">
                <h2></h2>
            </div>
            <div class="answer_statistic">
                <div class="correct">
                    <h3>Правильные ответы:</h3>
                    <div class="circle"></div>
                </div>
                <div class="mistakes">
                    <h3>Допушенно ошибок:</h3>
                    <div class="circle"></div>
                </div>
            </div>
            <button onclick="testingFunc();" class="next_btn">Пройти еще раз</button>
        </div>
    </div>
</main>
<?php
get_footer();
?>

<script>
<?php 
if (have_rows("questions_for_beginners")) :
    $questionsCount = 0;
?>
var questionsForBeginners = {
    questions: [
        <?php while(have_rows("questions_for_beginners")) : the_row(); ?>
        {
            title: "<?php the_sub_field("question"); ?>",
            answers: [
                { 
                    title: "<?php the_sub_field("first_answer"); ?>", 
                    weight: <?php the_sub_field("first_answer_weight"); ?> 
                },
                { 
                    title: "<?php the_sub_field("second_answer"); ?>", 
                    weight: <?php the_sub_field("second_answer_weight"); ?> 
                },
                { 
                    title: "<?php the_sub_field("third_answer"); ?>", 
                    weight: <?php the_sub_field("third_answer_weight"); ?> 
                },
                { 
                    title: "<?php the_sub_field("fourth_answer"); ?>", 
                    weight: <?php the_sub_field("fourth_answer_weight"); ?> 
                },
            ]
        },
        <?php $questionsCount++; endwhile; ?>
    ],
    count: <?php echo $questionsCount; ?>,
};
<?php endif; ?>

<?php 
if (have_rows("questions_for_advanced")) :
    $questionsCount = 0;
?>
var questionsForAdvanced = {
    questions: [
        <?php while(have_rows("questions_for_advanced")) : the_row(); ?>
        {
            title: "<?php the_sub_field("question"); ?>",
            answers: [
                { 
                    title: "<?php the_sub_field("first_answer"); ?>", 
                    weight: <?php the_sub_field("first_answer_weight"); ?> 
                },
                { 
                    title: "<?php the_sub_field("second_answer"); ?>", 
                    weight: <?php the_sub_field("second_answer_weight"); ?> 
                },
                { 
                    title: "<?php the_sub_field("third_answer"); ?>", 
                    weight: <?php the_sub_field("third_answer_weight"); ?> 
                },
                { 
                    title: "<?php the_sub_field("fourth_answer"); ?>", 
                    weight: <?php the_sub_field("fourth_answer_weight"); ?> 
                },
            ]
        },
        <?php $questionsCount++; endwhile; ?>
    ],
    count: <?php echo $questionsCount; ?>,
};
<?php endif; ?>



var questionsCurrent = questionsForBeginners;

var rating = 0;
var maxRating = 0;
var pos = 0;
var kol = questionsCurrent.questions.length;

for (var i = 0; i < questionsCurrent.questions.length; i++)
    for (var j = 0; j < questionsCurrent.questions[i].answers.length; j++)
        maxRating += questionsCurrent.questions[i].answers[j].weight;

function testingFunc(flag = true) {
    var block = document.querySelector(".test_block");
    if (!pos) flag = false;
    if (pos < kol) {
        var side = block.querySelector(".questions_side");
        var radios = side.querySelectorAll(".option .check");
        var answers = side.querySelectorAll(".option > span");

        side.querySelector(".current_number").textContent = pos + 1;
        side.querySelector(".total").textContent = kol;
        side.querySelector(".current_question span").textContent = "Вопрос " + (pos + 1) + " из " + kol;
        side.querySelector(".current_progress").style.width = (100 / kol) * (pos) + '%';
        side.querySelector(".question_wrap h2").textContent = questionsCurrent.questions[pos].title;

        for (var i = 0; i < radios.length; i++) {
            if (radios[i].checked && flag) {
                rating += questionsCurrent.questions[pos - 1].answers[i].weight;
            }

            answers[i].textContent = questionsCurrent.questions[pos].answers[i].title;
        }
        pos++;
        document.querySelector(".test_questions").style.display = 'flex';
        document.querySelector(".test_result").style.display = 'none';
    }
    else if (pos == kol) {
        var side = block.querySelector(".questions_side");
        var radios = side.querySelectorAll(".option .check");
        
        for (var i = 0; i < radios.length; i++) {
            if (radios[i].checked && flag) {
                rating += questionsCurrent.questions[pos - 1].answers[i].weight;
                break;
            }
        }
        document.querySelector(".correct .circle").innerHTML =
        '<div class="percent"> ' + (100 / maxRating * rating) + ' <span>%</span></div><svg width="160" height="160"><circle transform="rotate(-90)" r="65" cx="-80" cy="80" /><circle transform="rotate(-90)" style="stroke-dasharray:' + (100 / maxRating * rating * 408 / 100) + 'px 408px;" r="65" cx="-80" cy="80" /></svg>';
        document.querySelector(".mistakes .circle").innerHTML =
        '<div class="percent"> ' + (100 - 100 / maxRating * rating) + ' <span>%</span></div><svg width="160" height="160"><circle transform="rotate(-90)" r="65" cx="-80" cy="80" /><circle transform="rotate(-90)" style="stroke-dasharray:' + ((100 - 100 / maxRating * rating) * 408 / 100) + 'px 408px;" r="65" cx="-80" cy="80" /></svg>';
        document.querySelector(".test_questions").style.display = 'none';
        document.querySelector(".test_result").style.display = 'flex';

        if (100 / maxRating * rating > 50) {
            document.querySelector(".test_result").classList.remove("failed");
            document.querySelector(".test_result").classList.add("passed");
            document.querySelector(".test_result h2").textContent = "<?php the_field("passed_title"); ?>";
        }
        else {
            document.querySelector(".test_result").classList.add("failed");
            document.querySelector(".test_result").classList.remove("passed");
            document.querySelector(".test_result h2").textContent = "<?php the_field("failed_title"); ?>";
        }

        pos = 0;
        rating = 0;
    }
}

testingFunc(false);

function rewriteTestingFunc(n) {
    switch(n) {
        case 0:
            if (questionsCurrent != questionsForBeginners) {
                questionsCurrent = questionsForBeginners;
                pos = 0;
                testingFunc(false);
            }
            break;
        case 1:
            if (questionsCurrent != questionsForAdvanced) {
                questionsCurrent = questionsForAdvanced;
                pos = 0;
                testingFunc(false);
            }
            break;
    }
}

$('.category_item a').click(function() {
    $('.category_item').removeClass("current");
    $(this).parent().addClass("current");
})

</script>