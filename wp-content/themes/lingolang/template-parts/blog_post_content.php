
<div class="blog_item">
    <div class="image_block">
        <a href="<?php the_permalink(); ?>" class="blog_screen">
            <img src="<?php the_post_thumbnail_url(); ?>" alt="article">
        </a>
        <?php
            $test = wp_get_post_categories( $post->ID, array('fields' => 'all') );
            foreach( $test as $cat ){
                echo '<span class="category">' . $cat->name . '</span>';
            }
        ?>
    </div>
    <div class="blog_info">
        <div class="head_info">
            <div class="date"><span><?php the_time('d M Y'); ?></span></div>
            <div class="social">
                <div class="likes">
                    <svg width="11" height="9" viewBox="0 0 11 9" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                        <path d="M7.48673 0C6.93352 0 6.42634 0.175301 5.97929 0.521044C5.55069 0.852511 5.26534 1.27469 5.09735 1.58169C4.92935 1.27467 4.644 0.852511 4.2154 0.521044C3.76835 0.175301 3.26117 0 2.70796 0C1.16419 0 0 1.26273 0 2.93723C0 4.74627 1.4524 5.98398 3.65115 7.85772C4.02453 8.17592 4.44775 8.5366 4.88764 8.92127C4.94562 8.97204 5.02009 9 5.09735 9C5.1746 9 5.24907 8.97204 5.30705 8.92129C5.74698 8.53656 6.17018 8.1759 6.54378 7.85752C8.74229 5.98398 10.1947 4.74627 10.1947 2.93723C10.1947 1.26273 9.03051 0 7.48673 0Z"
                                />
                    </svg>
                    <span><?php echo do_shortcode('[wp_ulike]'); ?></span>
                </div>
                <div class="views">
                    <svg width="18" height="9" viewBox="0 0 18 9" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                        <path d="M16.9393 4.1665C16.7869 3.99657 13.1228 0 8.53335 0C3.94395 0 0.279817 3.99657 0.12745 4.1665C-0.0424833 4.35643 -0.0424833 4.64357 0.12745 4.8335C0.279817 5.00343 3.94402 9 8.53335 9C13.1227 9 16.7869 5.00343 16.9393 4.8335C17.1091 4.64357 17.1091 4.35643 16.9393 4.1665ZM8.53335 8C6.60365 8 5.03335 6.4297 5.03335 4.5C5.03335 2.5703 6.60365 1 8.53335 1C10.463 1 12.0333 2.5703 12.0333 4.5C12.0333 6.4297 10.463 8 8.53335 8Z"
                                />
                        <path d="M9.0332 3.5C9.0332 2.997 9.2829 2.5545 9.66277 2.28233C9.3219 2.10783 8.9417 2 8.5332 2C7.15477 2 6.0332 3.12157 6.0332 4.5C6.0332 5.87843 7.15477 7 8.5332 7C9.76734 7 10.7888 6.09887 10.9906 4.9214C9.98374 5.24557 9.0332 4.48407 9.0332 3.5Z"
                                />
                    </svg>
                    <span class="views"><?php if (function_exists ('the_views')) { the_views (); } ?></span>
                </div>
            </div>
        </div>
        <div class="blog_title">
            <h3><?php the_title(); ?></h3>
            <p class="short_description"><?php the_field("description"); ?></p>
        </div>
    </div>
</div>